﻿#pragma once
#include "Drive.h"
#pragma comment(lib, "comctl32.lib")

HWND CreateAListView(HWND hwndParent, long ID, HINSTANCE hInst, long lExtStyle, int x, int y, int nWidth, int nHeight, long lStyle);

void loadThisPCToList(Drive*, HWND);

void InitBasicCol(HWND);
void InitDriveCol(HWND);
void InitFolderCol(HWND);

LPCWSTR getPath(HWND, int); // Lấy đường dẫn từ đến tệp tin có chỉ mục

LPWSTR getDateModified(const FILETIME &);

void LoadFileAndFolder(HWND, LPCWSTR); // Lấy item đưa lên ListView

void LoadListViewItem(LPCWSTR path, HWND hListView, Drive* drive); // load từ đĩa lên

void OpenOrExecuteSelected(HWND); // mở folder hay thực thi file

BOOL InitListViewImageLists(HWND hWndListView);

void DisplayInfoCurSelect(HWND hListView, HWND hSttbar);

WCHAR* GetType(WIN32_FIND_DATA &fd);

## Thông tin cá nhân
### MSSV: 1512557
### Họ tên: Phan Trọng Thuyên
## Các chức năng đã làm được
1. Sử dụng shell để lấy các đối tượng thư mục và tập tin của các ổ đĩa.
2. Cải tiến cho phép khi thay đổi kích thước treeview thì listview thay đổi theo. 
3. Bổ  sung statusbar khi click vào một tập tin trong listview thì hiển thị kích thước tập tin tương ứng, thể hiện dạng động x.x KB/MB/GB/TB/PB
4. Lưu lại kích thước cửa sổ màn hình chính và nạp lại khi chương trình chạy lên.
## Các luồng sự kiện chính:
> Chạy chương trình, hiển thị treeview bên trái với root là ThisPC cùng 
các ổ đĩa là con của nó, bấm +/- để mở rộng hoặc thu gọn cây.
Ấn vào ổ đĩa khi ở trạng thái thu gọn sẽ xổ xuống các thư mục con tạo thành cây thư mục

>List view chứa chi tiết thư mục, ổ đĩa được chọn. Có các cột thông tin về tên, kích thước, loại, ngày chỉnh sửa gần nhất.
Select một tệp tin status bar sẽ hiển thị kích thước tệp tin đó
Double click vào thư mục trên listview sẽ mở thư mục đó và hiển thị các thư mục và tệp tin con của nó. Double click vào tệp tin sẽ mở tệp tin đó.
Thay đổi kích thước của TreeView thì ListView thay đổi. Khi exit hoặc nhấn close thì lưu kích thước cửa sổ để lần sau mở lên vẫn giữ kích thước đó
## Các luồng sự kiện phụ:
>Chương trình sẽ crash nếu double click vào vùng trống ở listview, do đó phải ràng buộc điều kiện khi double click.
## Các yêu cầu khác
- Nền tảng build: Visual Studio 2017
- Link bitbucket: https://billhcmus@bitbucket.org/billhcmus/windev.git
- Link Video: https://youtu.be/glSi-VskdY8
- Link repo: https://billhcmus@bitbucket.org/billhcmus/fileexplorerupgrade.git